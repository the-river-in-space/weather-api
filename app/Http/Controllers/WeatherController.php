<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\OpenWeatherMapService;
use App\Services\WeatherService;
use Illuminate\Http\Response;

// Now that I think about it, WeatherController sounds quite sinister.
class WeatherController extends Controller
{
    /**
     * @param string         $location
     * @param WeatherService $weatherService
     * @param Response       $response
     *
     * @return Response
     */
    public function getWeather(string $location, WeatherService $weatherService, Response $response): Response
    {
        return $response->setContent($weatherService->getWeather($location));
    }

    /**
     * @param string                $location
     * @param OpenWeatherMapService $openWeatherMapService
     * @param Response              $response
     *
     * @return Response
     */
    public function openWeatherMap(string $location, OpenWeatherMapService $openWeatherMapService, Response $response): Response
    {
        return $this->getWeather($location, $openWeatherMapService, $response);
    }
}
