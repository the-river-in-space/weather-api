<?php

declare(strict_types=1);

namespace App\Services;

use Cmfcmf\OpenWeatherMap;

class OpenWeatherMapService implements WeatherService
{
    public function __construct(private OpenWeatherMap $openWeatherMap) {}

    /**
     * @param string $forLocation
     *
     * @return OpenWeatherMap\CurrentWeather
     * @throws OpenWeatherMap\Exception
     */
    public function getWeather(string $forLocation): OpenWeatherMap\CurrentWeather
    {
        return $this->openWeatherMap->getWeather($forLocation, 'metric');
    }
}
