<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\OpenWeatherMapService;
use Cmfcmf\OpenWeatherMap;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\HttpFactory;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class OpenWeatherMapServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OpenWeatherMapService::class, function (Application $app): OpenWeatherMapService {
            $owm = new OpenWeatherMap(
                env('OPEN_WEATHER_MAP_API_KEY'),
                new Client(),
                new HttpFactory(),
            );

            return new OpenWeatherMapService($owm);
        });
    }

    public function provides()
    {
        return [OpenWeatherMapService::class];
    }
}
